<!DOCTYPE html>
<html>
<head>
    <title>Example One</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
</head>
<body>

<h2>Example One</h2>

<div id="chart_div" style="width: 600px; height: 300px;"></div>

<script>
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

        var rawData = [
            ['Year', 'Sales', 'Expenses'],
            ['2004',  1000,      400],
            ['2005',  1170,      460],
            ['2006',  660,       1120],
            ['2007',  1030,      540]
        ];

        // Convert the raw data into a Google Charts Data Table object
        var chartData = google.visualization.arrayToDataTable(rawData);

        // Specify some options about how the chart is to be drawn
        var chartOptions = {
            title: "Company Performance",
            hAxis: {title: "Year"},
        };

        // This is where we want to put the chart
        var chartDiv = document.getElementById("chart_div");

        // Create the Chart
        var chart = new google.visualization.ColumnChart(chartDiv);

        // Ask the Chart to draw itself with these data and options
        chart.draw(chartData, chartOptions);
    }
</script>
</body>
</html>