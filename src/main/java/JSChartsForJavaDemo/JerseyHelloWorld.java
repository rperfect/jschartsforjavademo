package JSChartsForJavaDemo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;

/**
 */
@Path("/HelloResource/{helloMsgParam}")
public class JerseyHelloWorld {


    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public String helloWorldMethod(@PathParam("helloMsgParam") String helloMsgParam) {
        return "Hello " + helloMsgParam + " at " + new Date();
    }
}
