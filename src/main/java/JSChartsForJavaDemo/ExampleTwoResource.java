package JSChartsForJavaDemo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Random;

/**
 */
@Path("/ExampleTwoResource/{startingYear}")
public class ExampleTwoResource {

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Object[] getMethod(@PathParam("startingYear") Integer startingYear) {

        System.out.println("ExampleTwoResource: startingYear = " + startingYear);

        // Declare the result array
        Object[] result = new Object[4 + 1];

        // Headings
        result[0] = new Object[] {"Year", "Sales", "Expenses"};

        // Just some random data based on using the input parameter
        Random random = new Random();
        for(int i = 0; i < 4; i++) {

            String year = String.valueOf(startingYear + i);
            int sales = (random.nextInt(100) + 42) * 100;
            int expenses = (random.nextInt(100) + 21) * 100;

            Object[] row = new Object[3];
            row[0] = year;
            row[1] = sales;
            row[2] = expenses;

            result[i + 1] = row;
        }

        return result;
    }

}
